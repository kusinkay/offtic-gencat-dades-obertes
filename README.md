# Gencat Dades Obertes

WordPress plugin for Generalitat de Catalunya's Open Data program

OpenStreetMaps Support
-
Using 4.6 version, the latest available in typescript definitions
https://openlayers.org/en/v4.6.5/examples/
run 

    npm install --save @types/openlayers

under vendor/openlayers folder
