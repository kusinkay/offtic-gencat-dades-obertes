<?php 
/**
 * View literal declaration to be addressed by javascript
 */
$class_ref->literals['cultura_select_location'] =              __('Select location', GCOD_PLUGIN_NAME);
$class_ref->literals['cultura_location_more_below'] =          __('(See more below)', GCOD_PLUGIN_NAME);
$class_ref->literals['cultura_no_results'] =                   __('Nothing found', GCOD_PLUGIN_NAME);
$class_ref->literals['cultura_entry_date'] =                   __('<b>Date: </b>', GCOD_PLUGIN_NAME);
$class_ref->literals['cultura_entry_links'] =                  __('<b>Links: </b>', GCOD_PLUGIN_NAME);
/**
 * translators: date interval form-to*/
$class_ref->literals['cultura_entry_from_to_date'] =           __('<b>Dates: </b>  from {0} to {1}', GCOD_PLUGIN_NAME);
$class_ref->literals['cultura_entry_timetable'] =              __('<b>Timetable: </b>', GCOD_PLUGIN_NAME);

?>
<noscript>
	<h2>
	<?php echo __('JavaScript disabled', OMMS_PLUGIN_NAME);?>
	</h2>
	<?php 
	//translators: it will embed the title of the module
	echo sprintf(__("You need to enable JavaScript in your browser so you can use '%s' system.", OMMS_PLUGIN_NAME), __("GenCat OpenData", OMMS_PLUGIN_NAME));
	?>
</noscript>
<div class="view" data-view>
  <div id="gcod-main" class="face is-active" data-face>
    <label for="txtQuery"><?php echo __('Words search', GCOD_PLUGIN_NAME);?></label>
    <input type="text" id="txtQuery" name="query">
    <label for="gcod_territori"><?php echo __('Location', GCOD_PLUGIN_NAME);?></label>
    <select id="gcod_territori"></select>
    <input type="hidden" id="gcod_territori_pre">
    <button id="btnSearch" class="button">cercar</button>
    <div id="gcod-map"></div>
    <div id="layList"></div>
    <form name ="qrForm" style="display:none">
    	<textarea rows="20" cols="20" name="msg" id="msg"></textarea>
    	<input type="text" name="t" value="0"/>
    	<input type="text" name="e" value="M"/>
    	<input type="text" name="m" value="Byte"/>
    	<input type="text" name="mb" value="UTF-8"/>
    </form>
    
    <div class="reveal tiny" id="dialog" data-reveal data-overlay="true">
      <div id="gcod_qr"></div>
      <br>
      <?php echo __('Capture QR code to instantly save the event to your calendar.', GCOD_PLUGIN_NAME);?>
      <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div><!-- //face -->
</div><!-- view -->