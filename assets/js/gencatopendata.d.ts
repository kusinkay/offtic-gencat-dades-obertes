/// <reference path="../../vendor/offtic/wpcommons/src/assets/js/wpcommons.d.ts" />
/// <reference path="../../vendor/openlayers/node_modules/@types/openlayers/index.d.ts" />
/// <reference path="../../vendor/offtic/wpcommons/src/typescript/jquery.d.ts" />
declare module Offtic.GenCatOpenData {
    class CulturaView extends Offtic.WPCommons.View {
        events: Array<any>;
        /**
         * prefiltered location*/
        location: string;
        map: ol.Map;
        layer: ol.layer.Vector;
        setViewName(): void;
        declareListeners(): void;
        getDates(entry: any): any;
        getLocations(depth?: number, id?: string): void;
        filterLocation(location: string): void;
        private locationToFormal(location);
        toICal(i: any): void;
        iCalDate(gencatdate: any): any;
    }
}
