��          t      �                 "     0     P     _  =   r     �     �     �     �  �  �     e     ~     �     �     �  C   �  	             *     ?        
      	                                              (See more below) <b>Date: </b> <b>Dates: </b>  from {0} to {1} <b>Links: </b> <b>Timetable: </b> Capture QR code to instantly save the event to your calendar. Location Nothing found Select location Words search Project-Id-Version: GenCat Open Data 0.0.0_BETA
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/offtic-gencat-dades-obertes
PO-Revision-Date: 2022-03-20 21:24+0100
Last-Translator: 
Language-Team: 
Language: ca_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=2; plural=(n != 1);
 (Més ubicacions a sota) <b>Dia: </b> <b>Dies: </b>  del {0} al {1} <b>Enllaços: </b> <b>Horari: </b> Captura el codi QR per guardar fàcilment l'event al teu calendari. Ubicació Sense resultats Selecciona ubicació Cerca per paraules 