<?php
use Offtic\wpcommons\Context;
use Offtic\wpcommons\Settings;
use Offtic\GenCatOpenData\Models\Territori;

/**
 * Plugin Name: GenCat Open Data
 * Description: WordPress plugin for Generalitat de Catalunya's Open Data program
 * Version: 0.0.0_BETA
 * Author: Juane Puig
 * Author URI: https://offtic.com
 * Text Domain: offtic-gencat-open-data
 */
defined ( 'ABSPATH' ) || exit ();

require_once 'vendor/autoload.php';

if (! defined ( 'GCOD_PLUGIN_NAME' )) {
    define ( 'GCOD_PLUGIN_NAME', 'offtic-gencat-open-data' );
}
if (! defined ( 'GCOD_PLUGIN_FILE' )) {
    define ( 'GCOD_PLUGIN_FILE', __FILE__ );
}

$context = new Context('gcod', GCOD_PLUGIN_NAME, GCOD_PLUGIN_FILE, '\\Offtic\\GenCatOpenData', __('GenCat OpenData', OMMS_PLUGIN_NAME) );
$context->add_module('Cultura');

$settings = new Settings($context);

$territori = new Territori($context);
$context->logger->debug( json_encode( $territori->get_tree() ) );


$context->load_clean_script( $context->acronim . '_qrcode' , '/assets/js/qrcode.js');
$context->load_clean_script( $context->acronim . '_qrcode_sjis' , '/assets/js/qrcode_SJIS.js');
$context->load_clean_script( $context->acronim . '_myqr' , '/assets/js/myqr.js');

$context->push_script('ol3_js' , '/assets/js/ol.js');
$context->push_style('ol3_css', '/assets/css/ol.css');

add_action( 'init', array($context, 'init') );
add_action( 'plugins_loaded', array($context, 'load_translation_files') );