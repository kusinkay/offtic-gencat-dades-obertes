<?php
namespace Offtic\GenCatOpenData\Modules;

use Offtic\wpcommons\Module;

class CulturaModule extends Module
{
    public function add_pages()
    {
        $this->add_page("cultura", "Agenda cultural", "agenda-cultural", "[{$this->context->acronim}_cultura]");
    }

    public function add_shortcodes()
    {
        if (function_exists("add_shortcode")){
            add_shortcode( "{$this->context->acronim}_cultura", array( $this, 'cultura' ) );
        }
    }
    
    function cultura($atts) {
        $this->add_template('cultura.php');
        $this->add_reveals();
        if ( isset($atts['location'])) {
            $this->add_instruction("oView.filterLocation('{$atts['location']}');");
        }
        return $this->run( __FUNCTION__ );
    }

}

