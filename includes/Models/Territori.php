<?php
namespace Offtic\GenCatOpenData\Models;

use Offtic\wpcommons\Context;

class Territori
{
    /**
     * 
     * @var Context
     */
    var $context;
    /**
     * @see https://analisi.transparenciacatalunya.cat/resource/6nei-4b44
     * @var string
     */
    const ENDPOINT = 'https://analisi.transparenciacatalunya.cat/resource/6nei-4b44.json';
    /**
     * 
     * @var Territori[]
     */
    var $children = [];
    
    /**
     * 
     * @var string see TipusTerritori
     */
    var $type = TipusTerritori::PROVINCIA;
    
    /**
     * 
     * @var string
     */
    var $id;
    
    var $fields = array(
        TipusTerritori::PROVINCIA => array(
            'id' => 'codiprov',
            'name' => 'provincia',
        ),
        TipusTerritori::COMARCA => array(
            'id' => 'codcomarca',
            'name' => 'comarca',
        ),
        TipusTerritori::MUNICIPI => array(
            'id' => 'codi_municipi',
            'name' => 'municipi',
        )
    );
    
    /**
     * 
     * @param Context $context
     * @param integer $type TipusTerritori
     */
    public function __construct( Context $context, $type = NULL, $id = NULL ) {
        if( $type != NULL ) {
            $this->type = $type;
        }
        $this->context = $context;
        $this->id = $id;
    }
    
    public function get_level( $recursive = FALSE ) {
        $id_field = $this->fields[$this->type]['id'];
        $name_field = $this->fields[$this->type]['name'];
        if ( $this->type > TipusTerritori::PROVINCIA ){
            $upper_id_field = $this->fields[$this->type - 1]['id'];
        }
        
        $distinct = implode(',', $this->fields[$this->type]);
        $query = '?$query=select distinct ' . $distinct;
        if ( $this->id != NULL ) {
            $query .= " where {$upper_id_field}='{$this->id}' ";
        }
        $query .= ' order by ' . $name_field;
        $query = str_replace(' ', '%20', $query);
        $this->context->logger->debug($query);
        $content = file_get_contents(self::ENDPOINT . $query);
        $data = json_decode($content, TRUE);
        $result = array();
        foreach ( $data as $node ) {
            if ( !empty($node)) {
                if ( $recursive && $this->type != TipusTerritori::MUNICIPI ) {
                    $inner_level = $this->type + 1;
                    $inner_id_field = $this->fields[$inner_level]['id'];
                    $territori = new Territori( $this->context, $inner_level, $node[$id_field] );
                    $node['children'] = $territori->get_level( $recursive );
                }
                $result[] = $node;
            }
        }
        return $result;
    }
    
    public function get_tree( ) {
        if ( $this->type == TipusTerritori::PROVINCIA && $this->is_cached()) {
            return json_decode($this->from_cache(), TRUE);
        } else {
            $result = $this->get_level( TRUE );
            if ($this->type == TipusTerritori::PROVINCIA) {
                $this->to_cache(json_encode($result));
            }
        }
        return $result;
    }
    
    private function is_cached() {
        return file_exists( $this->get_cache_filename() );
    }
    
    private function from_cache() {
        return file_get_contents($this->get_cache_filename());
    }
    
    private function to_cache( $contents ) {
        if ( !file_exists(dirname($this->get_cache_filename())) ) {
            mkdir( dirname($this->get_cache_filename()), null, true);
        }
        file_put_contents($this->get_cache_filename(), $contents);
    }
    
    private function get_cache_filename() {
        return dirname( $this->context->plugin_file ) .'/assets/data/' . 'territori.json';
    }
}

class TipusTerritori {
    const PROVINCIA = 0;
    const COMARCA = 1;
    const MUNICIPI = 2;
}

