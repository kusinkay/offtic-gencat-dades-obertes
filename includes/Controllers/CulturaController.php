<?php
namespace Offtic\GenCatOpenData\Controllers;

use Offtic\wpcommons\Controller;
use Offtic\GenCatOpenData\Models\Territori;

class CulturaController extends Controller
{
    function __construct($context) {
        $this->methods = array('territori');
        parent::__construct($context);
    }
    
    function territori() {
        $this->response(function( CulturaController $controller){
           $depth = (isset( $_REQUEST['depth'] ) ? $_REQUEST['depth'] : 0 );
           $id = $_REQUEST['id'];
           $output = array(
               'depth' => $depth,
               'locations' => array()
           );
           $territori = new Territori($this->context, $depth, $id);
           $output['locations'] = $territori->get_level();
           $output['id_field'] = $territori->fields[$depth]['id'];
           $output['name_field'] = $territori->fields[$depth]['name'];
           return $output;
        });
    }
}

