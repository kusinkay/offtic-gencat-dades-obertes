/// <reference path='../../vendor/offtic/wpcommons/src/assets/js/wpcommons.d.ts' />
/// <reference path='../../vendor/openlayers/node_modules/@types/openlayers/index.d.ts' />
/// <reference path='../../vendor/offtic/wpcommons/src/typescript/jquery.d.ts' />

module Offtic.GenCatOpenData {
    export class CulturaView extends Offtic.WPCommons.View {

        events: Array<any> = [];

        /**
         * prefiltered location*/
        location: string;

        map: ol.Map;
        layer: ol.layer.Vector;

        setViewName() {
            this.viewName = 'Cultura';
        }

        declareListeners() {
            var self = this;


            this.layer = new ol.layer.Vector( {
                source: new ol.source.Vector( {
                    features: [],
                } ),
                style: new ol.style.Style( {
                    image: new ol.style.Circle( {
                        radius: 9,
                        fill: new ol.style.Fill( { color: 'blue' } ),
                    } ),
                } ),
            } );

            self.map = new ol.Map( {
                target: document.getElementById( 'gcod-map' ),
                layers: [
                    new ol.layer.Tile( {
                        source: new ol.source.OSM()
                    } ),

                ],
                view: new ol.View( {
                    center: ol.proj.fromLonLat( [1.203, 41.566] ),
                    zoom: 7
                } )
            } );


            self.map.on( 'click', function( evt: any ) {
                if ( self.map.forEachFeatureAtPixel( evt.pixel,
                    function( feature: any ) {
                        if ( typeof feature == 'object' && typeof feature.getId == 'function' && feature.getId().indexOf( 'event_' ) == 0 ) {
                            document.location = ( '#' + feature.getId() );
                            return true;
                        }
                        return false;
                    } )
                ) {
                    /**/
                }
            } );

            self.map.addLayer( self.layer );
            self.map.renderSync();

            self.getLocations();

            jQuery( '#gcod_territori' ).change( function() {
                var depth: number = parseInt( jQuery( 'option:selected', this ).attr( 'data-depth' ) );
                if ( depth < 2 ) {
                    self.getLocations( depth + 1, jQuery( 'option:selected', this ).val() );
                }

            } );

            jQuery( "#gcod-show-map" ).click( function() {
                //jQuery( "#gcod-map" ).show();

            } );

            jQuery( '#btnSearch' ).click( function() {
                self.layer.getSource().clear();
                self.map.removeLayer( self.layer );
                jQuery( '#layList' ).html( "" );
                self.buttonStarted( jQuery( '#btnSearch' ) );
                var imatges;
                var srchFields = [
                    'denominaci',
                    'descripcio',
                    'subt_tol',
                    'tags_mbits',
                    'tags_categor_es',
                    'tags_altres_categor_es',
                    'comarca_i_municipi',
                    'espai',
                    'localitat',
                    'regi_o_pa_s'
                ];
                var aWhere = [];
                for ( var i = 0; i < srchFields.length; i++ ) {
                    aWhere.push( srchFields[i] + " LIKE '%" + jQuery( '#txtQuery' ).val() + "%'" );
                }
                var where = aWhere.join( " OR " );
                where = '(' + where + ')';

                var location: string = null;
                if ( jQuery( '#gcod_territori option:selected' ).attr( 'data-location-path' ) != null ) {
                    location = jQuery( '#gcod_territori option:selected' ).attr( 'data-location-path' );
                } else if ( jQuery( '#gcod_territori_pre' ).val() != "" ) {
                    location = jQuery( '#gcod_territori_pre' ).val();
                }
                if ( location != null ) {
                    where += " AND comarca_i_municipi LIKE '" + location + "%'";
                }

                var today = new Date();
                var sToday = today.getFullYear() + "-" + ( today.getMonth() + 1 ) + "-" + today.getDate()

                jQuery.ajax( {
                    url: "https://analisi.transparenciacatalunya.cat/resource/ta2y-snj2.json",
                    type: "GET",
                    data: {
                        "$limit": 5000,
                        "$where": "(" + where + ") AND data_fi>'" + sToday + "'"
                    }
                } ).done( function( data ) {
                    jQuery( '#layList' ).html( "" );
                    var pos = 0;
                    var content = "";
                    var codi = "";
                    var point: ol.geom.Point;
                    for ( var i = 0; i < data.length; i++ ) {
                        if ( codi == data[i].codi ) continue;
                        self.events.push( data[i] );
                        codi = data[i].codi;
                        if ( data[i].imatges != undefined ) {
                            imatges = data[i].imatges.split( "," )
                        }
                        var href = "#"
                        var hrefs = [];
                        if ( data[i].enlla_os != undefined ) {
                            hrefs = data[i].enlla_os.split( "," );
                            for ( var j = 0; j < hrefs.length; j++ ) {
                                if ( hrefs[j].indexOf( "gencat.cat" ) < 0 ) {
                                    href = hrefs[j];
                                    break;
                                }
                            }
                        }
                        content += "<div id='event_" + data[i].codi + "' style='clear:both'>"

                        content += "<hr><h3>" + data[i].denominaci + "</h3>"
                            + "<a data-gcod-qr='" + pos + "''>QR</a> - <a href='#gcod-map'>mapa</a><br><br>";
                        content += "<div class='gcod-entry'><a href='" + href + "' class='gcod-image'><img src='https://agenda.cultura.gencat.cat/" + imatges[0] + "'></a>" + data[i].descripcio_html + "</div>"
                            + data[i].comarca_i_municipi + "<br>"
                            + self.getDates( data[i] ) + "<br>"
                            + ( data[i].horari != null ? self.text( 'cultura_entry_timetable' ) + data[i].horari + "<br>" : '' )
                            + "<br/>";
                        if ( hrefs.length > 0 ) {
                            content += "<p>" + self.text( 'cultura_entry_links' ) + " <br>";
                            for ( var j = 0; j < hrefs.length; j++ ) {
                                content += "<a href='" + hrefs[j] + "'>" + ( hrefs[j].length > 100 ? hrefs[j].substring( 0, 100 ) + "..." : hrefs[j] ) + "</a><br>"
                            }
                            content += "</p>";
                        }
                        content += "</div>";
                        pos++;
                        var latitud = parseFloat( data[i].latitud );
                        var longitud = parseFloat( data[i].longitud );
                        if ( !isNaN( latitud ) && !isNaN( longitud ) ) {
                            point = new ol.geom.Point( ol.proj.fromLonLat( [longitud, latitud] ) );
                            var feature: ol.Feature = new ol.Feature( point );
                            feature.setId( 'event_' + data[i].codi );
                            self.layer.getSource().addFeature( feature );
                        }

                    }
                    self.map.addLayer( self.layer );
                    self.map.getView().fit( self.layer.getSource().getExtent() );
                    if ( self.layer.getSource().getFeatures().length == 1 ) {
                        self.map.getView().setZoom( 14 );
                    }
                    self.map.updateSize();

                    if ( data.length == 0 ) {
                        content = self.text( 'cultura_no_results' );
                    }
                    jQuery( '#layList' ).html( content );
                    self.buttonStopped( jQuery( '#btnSearch' ) );

                    jQuery( '[data-gcod-qr]' ).click( function() {
                        self.toICal( parseInt( jQuery( this ).attr( 'data-gcod-qr' ) ) );
                    } );
                    console.log( self.events );
                } );
            } );
        }

        getDates( entry: any ) {
            if ( entry.data_inici == entry.data_fi ) {
                var t: number = Date.parse( entry.data_inici );
                var d = new Date( t );
                if ( d != null )
                    return this.text( 'cultura_entry_date' ) + d.toLocaleDateString();
                else
                    return '';
            } else {
                return this.text( 'cultura_entry_from_to_date', [new Date( Date.parse( entry.data_inici ) ).toLocaleDateString(), new Date( Date.parse( entry.data_fi ) ).toLocaleDateString()] )
            }
        }

        getLocations( depth?: number, id?: string ) {
            var self = this;
            var parent: any;
            if ( depth != null ) {
                parent = jQuery( 'option[value="' + id + '"]' );
                var next = jQuery( parent ).next();
                if ( next != null ) {
                    var nDepth = parseInt( next.attr( 'data-depth' ) );
                    if ( !isNaN( nDepth ) && depth == nDepth ) {
                        /* children already loaded*/
                        return;
                    }
                }
                self.buttonStarted( jQuery( '[for="gcod_territori"]' ) );
            } else {
                self.showLoading();
            }
            var params: any = {};
            if ( depth != null ) {
                params.depth = depth;
            }
            if ( id != null ) {
                params.id = id;
            }

            this.backofficeCall( "gcod_territori", params, function( json ) {
                var indent = '';
                var location_path: string;
                var option;
                var parent: any;
                if ( depth > 0 ) {
                    parent = jQuery( 'option[value="' + id + '"]' );
                    indent = parent.html() + ' > ';
                    location_path = parent.attr( 'data-location-path' );
                } else {
                    option = jQuery( '<option>' );
                    jQuery( option ).attr( 'value', '' );
                    jQuery( option ).html( self.text( 'cultura_select_location' ) );
                    jQuery( '#gcod_territori' ).append( option );
                    location_path = 'agenda:ubicacions';
                }
                var previous = parent;
                for ( var i = 0; i < json.locations.length; i++ ) {
                    var location = json.locations[i];
                    option = jQuery( '<option>' );
                    jQuery( option ).attr( 'value', location[json.id_field] );
                    jQuery( option ).attr( 'data-depth', json.depth );
                    jQuery( option ).attr( 'data-location-path', location_path + '/' + self.locationToFormal( location[json.name_field] ) );
                    jQuery( option ).html( indent + location[json.name_field] );
                    if ( depth > 0 ) {
                        jQuery( option ).insertAfter( previous );
                        previous = option;
                    } else {
                        jQuery( '#gcod_territori' ).append( option );
                    }
                }

                if ( typeof params.loaded == 'function' ) {
                    params.loaded();
                }
            }, function() {
                /*error*/
            }, function() {
                /*complete*/

                if ( depth != null ) {
                    self.buttonStopped( jQuery( '[for="gcod_territori"]' ) );
                    if ( parent != null ) {
                        jQuery( parent ).html( jQuery( parent ).html() + ' ' + self.text( 'cultura_location_more_below' ) );
                    }
                } else {
                    self.hideLoading();
                }
            } );
        }

        filterLocation( location: string ) {
            this.location = location;
            if ( this.location != null ) {
                jQuery( '#gcod_territori' ).hide();
                jQuery( '[for=gcod_territori]' ).hide();
                jQuery( '#gcod_territori_pre' ).val( this.location );
                jQuery( '#btnSearch' ).trigger( 'click' );
            }
        }

        private locationToFormal( location: string ) {
            location = location.toLocaleLowerCase( 'en-US' );
            location = location.replace( /\s/g, '-' );
            location = location.replace( /'/g, '-' );
            location = location.normalize( "NFD" ).replace( /[\u0300-\u036f]/g, "" );
            return location;
        }

        toICal( i ) {
            var result = "BEGIN:VCALENDAR\n" +
                "VERSION:2.0\n" +
                "CALSCALE:GREGORIAN\n" +
                "BEGIN:VEVENT\n" +
                "SUMMARY:" + this.events[i].denominaci + "\n" +
                "DTSTART;TZID=Europe/Paris:" + this.iCalDate( this.events[i].data_inici ) + "\n" +
                "DTEND;TZID=Europe/Paris:" + this.iCalDate( this.events[i].data_fi ) + "\n" +
                "LOCATION:" + this.events[i].adre_a + "\n" +
                "DESCRIPTION:" + ( this.events[i].descripcio.length >= 250 ? this.events[i].descripcio.substr( 0, 250 ) + '...' : this.events[i].descripcio ) + "\n" +
                "STATUS:CONFIRMED\n" +
                "SEQUENCE:1\n" +
                "BEGIN:VALARM\n" +
                "TRIGGER:-PT10H\n" +
                "DESCRIPTION:Pickup Reminder\n" +
                "ACTION:DISPLAY\n" +
                "END:VALARM\n" +
                "END:VEVENT\n" +
                "END:VCALENDAR";
            jQuery( '#msg' ).val( result );
            update_qrcode();
            jQuery( '#dialog' ).foundation( 'open' );
            //return result;
        }


        iCalDate( gencatdate ) {
            var result = gencatdate.replace( /-/g, "" );
            result = result.replace( /:/g, "" );

            return result.substring( 0, 15 );
        }
    }
}